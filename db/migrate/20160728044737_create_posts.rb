class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :post_text
      t.boolean :is_public
      t.timestamps null: false
    end
  end
end

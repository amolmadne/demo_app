class Api::V1::UserPostsController < ApplicationController

	def create_post
		if params[:post_text].present? || params[:is_public].present? || params[:image].present?
			@post = Post.create(:post_text => params[:post_text] , :is_public => params[:is_public])
			@post.save
			@photo = Photo.create(:imageable_id => @post.id, :imageable_type => @post.class, :image_file_name => params[:image].present?)
			render :status => 200, :json => {:status=>"Success"}
		else
			render :status => 200, :json => {:status=>"Fail"}
		end
	end

	def show_post
		if params[:post_id].present?
			@post = Post.find(params[:post_id])
			if @post.present?
				@image = Photo.find_by_imageable_id(@post.id)
				render :status => 200, :json => {:status=>"Success", :post_text => @post.post_text, :is_public => @post.is_public, :image_url => @image.image_file_name}
			else
				render :status => 200, :json => {:status=>"Fail"}
			end
		else
			render :status => 200, :json => {:status=>"Fail"}
		end
	end

	def delete_post
		if params[:post_id].present?
			@post = Post.find(params[:post_id])
			if @post.present?
				@post.delete
				render :status => 200, :json => {:status=>"Success"}
			else
				render :status => 200, :json => {:status=>"Fail"}
			end
		else
			render :status => 200, :json => {:status=>"Fail"}
		end
	end

end

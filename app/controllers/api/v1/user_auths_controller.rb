class Api::V1::UserAuthsController < ApplicationController
	skip_before_action :authenticate_user!
	include ActionController::MimeResponds
	include ActionController::Cookies
	skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json

	def user_sign_up
		if request.format != :json
			render :status=>406, :json=>{:status=>"Failure",:message=>"The request must be json"}
			return
		end
		if params[:username].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"UserName is required"}
			return
		end
		if params[:first_name].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"First Name is required"}
			return
		end
		if params[:last_name].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"Last Name is required"}
			return
		end
		if params[:email].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"Email Address is required"}
			return
		else
			user_exist = User.find_by_email(params[:email])
			if user_exist.present?
				render  :status => 400,
								:json => {:status=>"Failure",:message=>"Email has already taken"}
				return
			end
		end
		if params[:password].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"Password is required"}
			return
		end
		@user = User.new({:email => params[:email],
					:username => params[:username],
					:first_name => params[:first_name],
					:last => params[:last_name],
					:password => params[:password]
								})
		if @user.save
			@user.confirm!
			sign_in @user, store: true

			render :status => 200, :json => { :status=>"Success",:username => @user.username,:email => @user.email, :first_name =>@user.first_name,:last_name => @user.last_name}
		else
			logger.info @user.errors.inspect
			render :status => 200, :json => { :status=>"Failure",:message=>"Registeration Failed Error: #{@user.errors.values.join(", ")}."}
		end
	end

	def user_sign_in
		if request.format != :json
			render :status=>406, :json=>{:status=>"Failure",:message=>"The request must be json"}
			return
		end

		email = params[:email]
		password = params[:password]

		if email.nil? or password.nil?
			render :status=>400,:json=>{:status=>"Failure",:message=>"The request must contain the email and password."}
			return
		end

		@user = User.find_by_email(email.downcase)
	 	if @user.nil?
			logger.info("User #{email} failed signin, user cannot be found.")
			render :status=>401, :json=>{:status=>"Failure",:message=>"Invalid email"}
			return
		end

		if not @user.valid_password?(password)
			logger.info("User #{email} failed signin, password \"#{password}\" is invalid")
			render :status=>401, :json=>{:status=>"Failure",:message=>"Invalid password."}
		else
			sign_in @user, store: true
			render :status=>200, :json=>{:status=>"Success",:email => @user.email,:user_id => @user.id}
		end
	end

	def facebook_authentication
		if request.format != :json
							render :status=>406, :json=>{:status=>"Failure",:message=>"The request must be json"}
			return
		end
		if params[:user_email].blank?
			render :status => 400,
				:json => {:status=>"Failure",:message=>"Email Address is required"}
			return
		end
		if params[:user_name].blank?
			render :status => 400,
				:json => {:status=>"Failure",:message=>"User name is required"}
			return
		end
		if params[:facebook_uid].blank?
			render :status => 400,
				:json => {:status=>"Failure",:message=>"Facebook UID is required"}
			return
		end
		if params[:registration_id].blank? or params[:mobile_type].blank?
			render  :status => 400,
							:json => {:status=>"Failure",:message=>"registration_id/mobile_type both required"}
			return
		end

		@user = User.find_by_email(params[:user_email]) rescue nil
		if @user.present?
			 @user.update_attributes(:registration_id=>params[:registration_id])
			render :status=>200, :json=>{:status=>"Success",:authentication_token=>@user.authentication_token,:user_id => @user.id, :first_name => @user.first_name, :email => @user.email}
			return
		elsif !@user.present?
			@user = User.new
			@user.first_name = params[:user_name]
			@user.provider = "facebook"
			@user.uid = params[:facebook_uid]
			@user.email = params[:user_email]
			@user.password = Devise.friendly_token[0,20]
			@user.authentication_token = params[:auth_token]
			@user.registration_id = params[:registration_id]
		end
		if @user.save
			@user.confirm!
			sign_in @user, store: true
			render :status=>200, :json=>{:status=>"Success",:authentication_token=>@user.authentication_token,:user_id => @user.id, :name => @user.first_name, :email => @user.email}
		else
			logger.info @user.errors.inspect
			render :status => 200, :json=>{ :status=>"Failure",:message=>"Registeration Failed Error: #{@user.errors.full_messages.join(", ")}."}
		end
	end

end

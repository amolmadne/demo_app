class Post < ActiveRecord::Base
  has_many :photos, :as => :imageable, dependent: :destroy
  accepts_nested_attributes_for :photos

  validates_presence_of :post_text, :is_public
end

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  has_many :photos, :as => :imageable, dependent: :destroy
  accepts_nested_attributes_for :photos

  validates :email,
            :uniqueness=> true,
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            :presence =>true

  validates :username,:presence => true,
            :format => { :with => /\A^[^0-9`"<>\[\];,?{}()~:!@#\$%\-\^&*+_=]+$\Z/,
            :message=> "Please Check First Name", :allow_blank => true},
            :length => { :minimum => 2, :maximum => 25 }
end

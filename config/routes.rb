Rails.application.routes.draw do
  devise_for :users, :controllers => { :passwords => "devise/passwords",
                                       :omniauth_callbacks => "devise/omniauth_callbacks"}
  root 'posts#index'
  resources :posts

  namespace :api do
    namespace :v1 do
      post 'user_auths/user_sign_up' => 'user_auths#user_sign_up', defaults: {format: 'json'}
      post 'user_auths/user_sign_in' => 'user_auths#user_sign_in', defaults: {format: 'json'}
      post 'user_posts/create_post' => 'user_posts#create_post', defaults: {format: 'json'}
      get 'user_posts/show_post' => 'user_posts#show_post', defaults: {format: 'json'}
      get 'user_posts/delete_post' => 'user_posts#delete_post', defaults: {format: 'json'}
    end
  end
end
